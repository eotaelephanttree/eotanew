﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ElephanttreeOnlineCourses.Models;
using Zender.Mail;
using System.Net.Mail;
using System.Web.Routing;
using System.Net;
using System.Net.NetworkInformation;
using System.Web.Security;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.text.html.simpleparser;

namespace ElephanttreeOnlineCourses.Controllers
{
    [RequireHttps]
    [HandleError]
    [ValidateInput(false)]
    public class HomeController : Controller
    {
        public ActionResult Index2()
        {
            ViewBag.name = "Indranil Ganguly";
            ViewBag.sub = ".net MVC";
            ViewBag.dt = DateTime.Now.ToShortDateString();
            var dateAndTime = DateTime.Now;
            DateTime date = dateAndTime.Date;
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public FileResult Export(string GridHtml, string GridHtml1)
        {
            using (MemoryStream stream = new System.IO.MemoryStream())
            {
                string imageURL = Server.MapPath(".") + "/img771.jpg";
                Image jpg = Image.GetInstance(imageURL);
                GridHtml += string.Format("<div><div><div style='padding-top: 10px; padding-left: 40px; '> <img  src='http://localhost:28493/Home/img771.jpg'></img></div></div></div> ");

                GridHtml += GridHtml1;
                StringReader sr = new StringReader(GridHtml);


                //Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
                Rectangle envelope = new Rectangle(580, 420);
                envelope.BackgroundColor = new BaseColor(System.Drawing.Color.LightYellow);
                envelope.BorderColorLeft = BaseColor.CYAN;
                Document pdfDoc = new Document(envelope, 15f, 15f, 15f, 5f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                //string imageURL = Server.MapPath(".") + "/close.png";
                //Image jpg = Image.GetInstance(imageURL);
                jpg.ScaleToFit(140f, 120f);

                jpg.SpacingAfter = 1f;
                jpg.Alignment = Element.ALIGN_LEFT;
                //pdfDoc.Add(jpg);
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);

                pdfDoc.Close();
                return File(stream.ToArray(), "application/pdf", "Certificate.pdf");
            }
        }

        public ActionResult Index1(string id)
        {
            return View();
        }
        [ValidateInput(false)]
        public ActionResult SearchAll()
        {
            FormsAuthentication.SignOut();
           
                using (EOTAEntities dbModel = new EOTAEntities())
                {
                   
                    ViewBag.academiccourses = dbModel.Post_Courses.Where(x => x.Course_Type == "A" && x.EnableDisable == true).OrderBy(x => x.Secret_Code).Distinct().ToList();
                    ViewBag.technicalcourses = dbModel.Post_Courses.Where(x => x.Course_Type == "T" && x.EnableDisable == true).OrderBy(x => x.Secret_Code).Distinct().ToList();
                    ViewBag.managementcourses = dbModel.Post_Courses.Where(x => x.Course_Type == "M" && x.EnableDisable == true).OrderBy(x => x.Secret_Code).Distinct().ToList();
                    return View();

                }

          
        }
        [ValidateInput(false)]
        public ActionResult SearchOne(string id)

        {
            FormsAuthentication.SignOut();
            if (id != null)
            {
                using (EOTAEntities dbModel = new EOTAEntities())
                {
                    ViewBag.aca = dbModel.Post_Courses.Where(x => x.EnableDisable == true && x.Course_Type == "A").OrderBy(x => x.Secret_Code).Distinct().ToList();
                    ViewBag.tech = dbModel.Post_Courses.Where(x => x.EnableDisable == true && x.Course_Type == "T").OrderBy(x => x.Secret_Code).Distinct().ToList();
                    ViewBag.management = dbModel.Post_Courses.Where(x => x.EnableDisable == true && x.Course_Type == "M").OrderBy(x => x.Secret_Code).Distinct().ToList();
                    if(id=="A")
                    {
                        ViewBag.CourseType = "A";
                        ViewBag.searchname = "Academic Courses";
                    }else if(id=="T")
                    {
                        ViewBag.CourseType = "T";
                        ViewBag.searchname = "Technical Courses";
                    }
                    else if (id == "M")
                    {
                        ViewBag.CourseType = "M";
                        ViewBag.searchname = "Management Courses";
                    }
                    else
                    {
                        ViewBag.CourseType = "N";
                        ViewBag.searchname = "Selected course cannot be found";
                    }
                    ViewBag.academiccourses = dbModel.Post_Courses.Where(x => x.Course_Type == "A" && x.EnableDisable == true).OrderBy(x => x.Secret_Code).Distinct().ToList();
                    ViewBag.technicalcourses = dbModel.Post_Courses.Where(x => x.Course_Type == "T" && x.EnableDisable == true).OrderBy(x => x.Secret_Code).Distinct().ToList();
                    ViewBag.managementcourses = dbModel.Post_Courses.Where(x => x.Course_Type == "M" && x.EnableDisable == true).OrderBy(x => x.Secret_Code).Distinct().ToList();
                    return View();

                }

            }
            else
            {
                return RedirectToAction("Index", new RouteValueDictionary(
                     new { controller = "Home", action = "Index" }));
            }
        }

        public ActionResult singlecrs(string id)
        {
            FormsAuthentication.SignOut();
            using (Models.EOTAEntities dbModel = new Models.EOTAEntities())
            {
                ViewBag.ss = dbModel.Post_Courses.Distinct()
     .Where(i => i.Course_Type == "T")
     .ToArray();
                ViewBag.Techcourse = (from a in dbModel.Post_Courses
                                      where a.Course_Type == "T" && a.EnableDisable == true
                                      select a.Title).Distinct().ToList();

                ViewBag.Acacourse = (from a in dbModel.Post_Courses
                                     where a.Course_Type == "A" && a.EnableDisable == true
                                     select a.Title).Distinct().ToList();

                ViewBag.Mngcourse = (from a in dbModel.Post_Courses
                                     where a.Course_Type == "M" && a.EnableDisable == true
                                     select a.Title).Distinct().ToList();
                ViewBag.secretcode = id;
                var id1 = int.Parse(id);
                ViewBag.aca = dbModel.Post_Courses.Where(x => x.EnableDisable == true && x.Course_Type == "A").OrderBy(x => x.Secret_Code).Distinct().ToList();
                ViewBag.tech = dbModel.Post_Courses.Where(x => x.EnableDisable == true && x.Course_Type == "T").OrderBy(x => x.Secret_Code).Distinct().ToList();
                ViewBag.management = dbModel.Post_Courses.Where(x => x.EnableDisable == true && x.Course_Type == "M").OrderBy(x => x.Secret_Code).Distinct().ToList();
                

                ViewBag.selectcrs = dbModel.Post_Courses.Where(x => x.Secret_Code == id1 && x.EnableDisable == true).Distinct().ToList();
                
                return View();
            }
        }
       
        public ActionResult Search( string search)

        {
            FormsAuthentication.SignOut();
            if (search.Trim().Length != 0 )
            {
                using (EOTAEntities dbModel = new EOTAEntities())
                {
                    ViewBag.aca = dbModel.Post_Courses.Where(x => x.EnableDisable == true && x.Course_Type == "A").OrderBy(x => x.Secret_Code).Distinct().ToList();
                    ViewBag.tech = dbModel.Post_Courses.Where(x => x.EnableDisable == true && x.Course_Type == "T").OrderBy(x => x.Secret_Code).Distinct().ToList();
                    ViewBag.management = dbModel.Post_Courses.Where(x => x.EnableDisable == true && x.Course_Type == "M").OrderBy(x => x.Secret_Code).Distinct().ToList();
                    ViewBag.searchname = search;
                    ViewBag.academiccourses = dbModel.Post_Courses.Where(x => x.Title.StartsWith(search) && x.Course_Type == "A" && x.EnableDisable == true).OrderBy(x => x.Secret_Code).Distinct().ToList();
                    ViewBag.technicalcourses = dbModel.Post_Courses.Where(x => x.Title.StartsWith(search) && x.Course_Type == "T" && x.EnableDisable == true).OrderBy(x => x.Secret_Code).Distinct().ToList();
                    ViewBag.managementcourses = dbModel.Post_Courses.Where(x => x.Title.StartsWith(search) && x.Course_Type == "M" && x.EnableDisable == true).OrderBy(x => x.Secret_Code).Distinct().ToList();
                    return View();

                }
                
            }else
            {
                return RedirectToAction("Index", new RouteValueDictionary(
                     new { controller = "Home", action = "Index" }));
            }
        }
       
        public ActionResult Index(string id)
        {
            FormsAuthentication.SignOut();
            using (EOTAEntities dbModel = new EOTAEntities())
            {
                //byte[] encData_byte = new byte[9];
                //encData_byte = System.Text.Encoding.UTF8.GetBytes("123654789");
                //string encodedData = Convert.ToBase64String(encData_byte);
                var discount = (from s in dbModel.Discount_Calculators
                               where s.Disable_Date>DateTime.Now
                                     select new {s.Discount_Code,s.Disable_Date,s.Discount_Price}).Distinct().FirstOrDefault();
                //System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
                //System.Text.Decoder utf8Decode = encoder.GetDecoder();
                //byte[] todecode_byte = Convert.FromBase64String(encodedData);
                //int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
                //char[] decoded_char = new char[charCount];
                //utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
                //string resultfvffbfb = new String(decoded_char);
                if (discount!=null)
                {
                    if (discount.Disable_Date < DateTime.Now)
                    {
                        ViewBag.disabledt = (discount.Disable_Date).Value.Minute;
                        ViewBag.discocde = null;
                        ViewBag.disper = null;
                    }
                    else
                    {
                        ViewBag.disabledt = (discount.Disable_Date - DateTime.Now).Value.TotalMinutes;
                        ViewBag.discocde = discount.Discount_Code;
                        ViewBag.disper = discount.Discount_Price;
                    }
                }
                ViewBag.aca = dbModel.Post_Courses.Where(x => x.EnableDisable == true && x.Course_Type == "A").OrderBy(x => x.Secret_Code).Distinct().ToList();
                ViewBag.tech = dbModel.Post_Courses.Where(x => x.EnableDisable == true && x.Course_Type == "T").OrderBy(x => x.Secret_Code).Distinct().ToList();
                ViewBag.management = dbModel.Post_Courses.Where(x => x.EnableDisable == true && x.Course_Type == "M").OrderBy(x => x.Secret_Code).Distinct().ToList();
               
                ViewBag.academiccourses = dbModel.Post_Courses.Where(x =>  x.Course_Type == "A" && x.EnableDisable == true).OrderBy(x => x.Secret_Code).Distinct().ToList();
                ViewBag.technicalcourses = dbModel.Post_Courses.Where(x =>  x.Course_Type == "T" && x.EnableDisable == true).OrderBy(x => x.Secret_Code).Distinct().ToList();
                ViewBag.managementcourses = dbModel.Post_Courses.Where(x =>  x.Course_Type == "M" && x.EnableDisable == true).OrderBy(x => x.Secret_Code).Distinct().ToList();
                var usrcntr1 = (from s in dbModel.Use_Test_counters
                                select s).Distinct().ToList();

                var usrcntr = (
                               from sa in dbModel.Use_Mock_Tests

                               select sa).Distinct().ToList();
                if(id=="ok")
                {
                    ViewBag.errrid = "15";
                }
                if (id == "oksub")
                {
                    ViewBag.errrid = "16";
                }
                if (id == "23")
                {
                    ViewBag.errrid = "23";
                }
                if (id == "1")
                {
                    ViewBag.errrid = "1";
                }
                if (id == "2")
                {
                    ViewBag.errrid = "2";
                }
                if (id == "3")
                {
                    ViewBag.errrid = "3";
                }
                if (id == "4")
                {
                    ViewBag.errrid = "4";
                }
                if (id == "5")
                {
                    ViewBag.errrid = "5";
                }
                if (id == "6")
                {
                    ViewBag.errrid = "6";
                }
                if (id == "7")
                {
                    ViewBag.errrid = "7";
                }
                if (id != null)
                {
                    string id1 = (id.Substring(id.LastIndexOf("]") + 1));
                    var iis = id.Contains("]");
                    if (iis == true)
                    {
                        string userid54544 = (id.Substring(0, id.LastIndexOf("]") + 0));
                        if (userid54544 == "12")
                        {
                            ViewBag.errrid = "12";
                            ViewBag.errrmsg14 = id1;
                        }
                        if (userid54544 == "13")
                        {
                            ViewBag.errrid = "13";
                            ViewBag.errrmsg14 = id1;
                        }
                        if (userid54544 == "14")
                        {
                            ViewBag.errrid = "14";
                            ViewBag.errrmsg14 = id1;
                        }
                    }
                }
                var result = usrcntr.Where(p => !usrcntr1.Any(p2 => p2.Exam_Secretcode == p.Exam_Secretcode)).ToList();
               ViewBag.feedback = (from a in dbModel.Feedbacks
                                where a.IsActive == true
                                orderby a.Id descending
                                select a).Take(3).ToList();
                ViewBag.usrnm = (from b in dbModel.Tbl_Cand_Main
                                  
                                    select b).ToList();
                foreach (var vp in result)
                {
                    dbModel.Use_Mock_Tests.Remove(vp);
                    dbModel.SaveChanges();
                }
            }
            id = null;
            return View();
          
        }
        [HttpGet]
        public ActionResult termscond()
        {
            return View();
        }
        //[HttpPost]
        //public ActionResult Mailsender(FormCollection frm)
        //{
        //   var name = frm["name"];
        //    var email = frm["email"];
        //    var tel1 = frm["tel"].ToString();
        //    var Coursetype = frm["CRSTP"];
        //    string Coursetypetxt = null;
        //    if (Coursetype=="A")
        //    {
        //        Coursetypetxt = "Academic";
        //    }
        //    else if (Coursetype == "T")
        //    {
        //        Coursetypetxt = "Technical";
        //    }
        //    else if (Coursetype == "M")
        //    {
        //        Coursetypetxt = "Management";
        //    }
        //    else
        //    {
        //        Coursetypetxt = "Does not exists";
        //    }
            
        //    var other = frm["other"];
        //    var Message = frm["Message"];
        //    SendEmail(name, email, tel1, Coursetypetxt, other, Message);
            
        //    return RedirectToAction("Index", new RouteValueDictionary(
        //             new { controller = "Home", action = "Index", Id = "ok" }));
        //}

        [HttpPost]
        public ActionResult Subscribe(string Email)
        {
            string message = "Dear Sir/Madam," +
                "<br/>I have keen interest in EOTA. Please share me all latest updates of EOTA." +
                "<br/>Thank you";
            SendEmail(Email);
            SendEmail(Email,message);
            return RedirectToAction("Index", new RouteValueDictionary(
                     new { controller = "Home", action = "Index", Id = "oksub" }));
        }

        [NonAction]
        public void SendEmail( string email,string Message1)
        {
            ZenderMessage message = new ZenderMessage("eaf7f170-f8e8-4391-8e5b-eb1919fca608");

            MailAddress from = new MailAddress("etreetraining@gmail.com");
           
            MailAddress to = new MailAddress("etreetraining@gmail.com");

            message.From = from;

            message.To.Add(to);
            using (EOTAEntities dbModel = new EOTAEntities())
            {
               
                var server = Request.Url.Segments;
               
                message.Subject = "User subscription";

                message.Body = "<br/>User email: " + email +

                    "<br/>" + Message1;
                 
                message.IsBodyHtml = true;

                message.SendMailAsync();

            }

        }

        [NonAction]
        public void SendEmail(string email)
        {
            ZenderMessage message = new ZenderMessage("eaf7f170-f8e8-4391-8e5b-eb1919fca608");

            MailAddress from = new MailAddress("etreetraining@gmail.com");
           
            MailAddress to = new MailAddress(email);

            message.From = from;

            message.To.Add(to);
            using (EOTAEntities dbModel = new EOTAEntities())
            {

                var server = Request.Url.Segments;

                message.Subject = "EOTA subscription successfull";

                message.Body = "Thank You for your subscription." +
                    "<br/><br/>You will recieve our latest news";
                    

                message.IsBodyHtml = true;

                message.SendMailAsync();

            }

        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}