
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 12/12/2017 12:43:09
-- Generated from EDMX file: C:\Users\user\Desktop\work\ElephanttreeOnlineCourses\ElephanttreeOnlineCourses\Models\TrainningAllModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [EOTA];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Tbl_Doc]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Tbl_Doc];
GO
IF OBJECT_ID(N'[dbo].[Tbl_Video]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Tbl_Video];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Tbl_Doc'
CREATE TABLE [dbo].[Tbl_Doc] (
    [Doc_id] int IDENTITY(1,1) NOT NULL,
    [Doc_Title] varchar(250)  NULL,
    [Doc_Path] varchar(max)  NULL,
    [Doc_ImagePath] nvarchar(max)  NULL
);
GO

-- Creating table 'Tbl_Video'
CREATE TABLE [dbo].[Tbl_Video] (
    [Video_id] int IDENTITY(1,1) NOT NULL,
    [Video_Title] varchar(250)  NULL,
    [Video_Path] varbinary(max)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Doc_id] in table 'Tbl_Doc'
ALTER TABLE [dbo].[Tbl_Doc]
ADD CONSTRAINT [PK_Tbl_Doc]
    PRIMARY KEY CLUSTERED ([Doc_id] ASC);
GO

-- Creating primary key on [Video_id] in table 'Tbl_Video'
ALTER TABLE [dbo].[Tbl_Video]
ADD CONSTRAINT [PK_Tbl_Video]
    PRIMARY KEY CLUSTERED ([Video_id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------